export const state = () => ({
    items: [
        { 
          transaction_number:'02-111419-00001',
          plate_number: "SJK-123",
          truck_model: "Isuzu Travis",
          driver_name: "Rey Reyes",
    
          supplier_name: "Revive Croptech Inc.",
          supplier_add: "Koronadal, South Cotabato",
    
          transaction_type: "Delivery",
          raw_material: "Corn",
         },
         { 
          transaction_number:'02-111419-00002',
          plate_number: "XRM-2187",
          truck_model: "Isuzu Travis",
          driver_name: "John Simson",
    
          supplier_name: "Revive Croptech Inc.",
          supplier_add: "Koronadal, South Cotabato",
    
          transaction_type: "Delivery",
          raw_material: "Corn",
         },
         { 
          transaction_number:'02-111419-00003',
          plate_number: "SJK-123",
          truck_model: "Isuzu Travis",
          driver_name: "Peter Piper",
    
          supplier_name: "Revive Croptech Inc.",
          supplier_add: "Koronadal, South Cotabato",
    
          transaction_type: "Delivery",
          raw_material: "Corn",
         },
         { 
          transaction_number:'02-111419-00004',
          plate_number: "SJK-123",
          truck_model: "Isuzu Travis",
          driver_name: "Mike Jacinto",
    
          supplier_name: "Revive Croptech Inc.",
          supplier_add: "Koronadal, South Cotabato",
    
          transaction_type: "Delivery",
          raw_material: "Corn",
         },
      ],
  })
  
  export const mutations = {
    add (state, text) {
      
    }
  }